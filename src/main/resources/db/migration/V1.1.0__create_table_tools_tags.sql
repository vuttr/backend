CREATE TABLE IF NOT EXISTS tools_tags (
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL,
    tools_id INT NOT NULL
);

ALTER TABLE tools_tags
ADD CONSTRAINT tools_tags_tools_id_fk
FOREIGN KEY (tools_id) REFERENCES tools;
