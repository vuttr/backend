# VUTTR (Very Useful Tools to Remember)

[![coverage report](https://gitlab.com/walber.oliveira/vuttr/badges/%{default_branch}/coverage.svg)](https://gitlab.com/walber.oliveira/vuttr/-/commits/%{default_branch})
[![pipeline status](https://gitlab.com/walber.oliveira/vuttr/badges/%{default_branch}/pipeline.svg)](https://gitlab.com/walber.oliveira/vuttr/-/commits/%{default_branch})

Este projeto utiliza Quarkus, o Supersonic Subatomic Java Framework.

Se quiser aprender mais sobre Quarkus, visite o site: https://quarkus.io/

## Requisitos - Desenvolvimento

* [JDK 11+](https://adoptopenjdk.net/installation.html)
* [Docker](https://docs.docker.com/engine/install/)
* [DockerCompose](https://docs.docker.com/compose/install/)

Lembre-se que é necessário configurar a variável de ambiente `JAVA_HOME` em seu sistema operacional.

## Executando a aplicação em modo DEV

Para executar a aplicação em modo DEV com hot-reload utilize o seguinte comando:

```
./mvnw quarkus:dev
```

Para abrir a documentação do Swagger, é necessário estar em modo DEV e acessar http://localhost:3000/docs

## Executando os tests

Para executar os testes, utilize o comando:

```
./mvnw clean verify
``` 

É possível abrir o HTML para verificar a cobertura no diretório:

```target/coverage/index.hmtml```

## Compilando e executando

Existem algumas formas para efetuar a compilação e execução. Escolha aquela que lhe agrada =)

* Compilando JAR e executando pela JRE

  Para compilar a aplicação (com execução dos testes), segue o comando:

  `./mvnw clean package`

  Obs: para compilar a aplicação sem testes, segue o comando:

  `./mvnw clean package -DskipTests`

  Para executar a aplicação, segue o comando:

  `java -jar target/vuttr-*-runner.jar`

* Compilando JAR e executando via CONTAINER
  Para compilar a aplicação (com execução dos testes), segue o comando:

  `./mvnw clean package -Dquarkus.container-image.build=true`

  Obs: para compilar a aplicação sem testes, segue o comando:

  `./mvnw clean package -Dquarkus.container-image.build=true -DskipTests`

  Para executar a aplicação, segue o comando:

  `docker run -p 3000:3000 vuttr.com/backend:dev`

* Compilando e executando nativo
  ### Requisitos
    * [GraalVM](https://www.graalvm.org/getting-started/#install-graalvm)
    * [GraalVM - NativeImage](https://www.graalvm.org/getting-started/#native-images)

  Para compilar a aplicação (com execução dos testes), segue o comando:

  `./mvnw clean package -Pnative`

  Obs: para compilar a aplicação sem testes, segue o comando:

  `./mvnw clean package -Pnative -DskipTests`

  Para executar a aplicação, segue o comando:

  `./target/vuttr-2.0.2-runner`

  [Quer saber mais sobre o build de executáveis nativos](https://quarkus.io/guides/building-native-image)

## Acessando

Após escolher a melhor maneira de compilar e executar, para acessar a documentação,
acesse:

[http://localhost:3000/docs](http://localhost:3000/docs)

## Versões

Para novas versões, acesse as [tags no repository.](https://gitlab.com/walber.oliveira/vuttr/-/tags)

## Autor

**Walber Junior de Oliveira** - *Construção do projeto*
